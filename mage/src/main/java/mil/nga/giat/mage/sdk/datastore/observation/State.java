package mil.nga.giat.mage.sdk.datastore.observation;

/**
 * The State of an {@link Observation}
 */
public enum State {
	ACTIVE, COMPLETE, ARCHIVE
}
